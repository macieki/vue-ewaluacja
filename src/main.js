import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueDragscroll from "vue-dragscroll";
Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(VueDragscroll)

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')


setTimeout(() => {
	
	const ele = document.getElementsByClassName("v-data-table__wrapper")[0];
	console.log(ele)
	let pos = { top: 0, left: 0, x: 0, y: 0 };
	const mouseDownHandler = function (e) {
		pos = {
			// The current scroll
			left: ele.scrollLeft,
			top: ele.scrollTop,
			// Get the current mouse position
			x: e.clientX,
			y: e.clientY,
		};
		ele.style.cursor = 'grabbing';
		ele.style.userSelect = 'none';
		document.addEventListener('mousemove', mouseMoveHandler);
		document.addEventListener('mouseup', mouseUpHandler);
	};

	const mouseMoveHandler = function (e) {
		// How far the mouse has been moved
		const dx = e.clientX - pos.x;
		const dy = e.clientY - pos.y;

		// Scroll the element
		ele.scrollTop = pos.top - dy;
		ele.scrollLeft = pos.left - dx;
	};

	const mouseUpHandler = function () {
		document.removeEventListener('mousemove', mouseMoveHandler);
		document.removeEventListener('mouseup', mouseUpHandler);

		ele.style.cursor = 'grab';
		console.log(ele.style.cursor)
		ele.style.removeProperty('user-select');
	};
	ele.addEventListener('mousedown', mouseDownHandler);

}, 3000);

